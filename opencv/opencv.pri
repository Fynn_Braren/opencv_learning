INCLUDEPATH += ../opencv
HEADERS += ../opencv/cvmattoqimage.h

win32 {
#  f�r Windows muss die Umgebungsvariable OPENCV_DIR gesetzt sein
#  auf den Pfad der OpenCV-Installation
#  Beispiel: C:\opencv\opencv-3.1.0\opencv\build\x64\vc14

    INCLUDEPATH += $$(OPENCV_DIR)\..\..\include
    LIBS += -L$$(OPENCV_DIR)\lib
    Release:LIBS +=  -lopencv_world330
    Debug:LIBS +=  -lopencv_world330d
}
