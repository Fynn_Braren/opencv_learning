#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <opencv2/opencv.hpp>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    void createImage(cv::Mat& image);
    void showImage(const cv::Mat& image);
    void getPixelValue(cv::Mat &image, int y, int x);

};

#endif // MAINWINDOW_H
