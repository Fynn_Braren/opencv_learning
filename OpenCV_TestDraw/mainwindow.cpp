#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <iostream>

#include "opencv2/opencv.hpp"
#include "cvmattoqimage.h"
using namespace cv;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // leeres Bild definieren
    Mat image(400, 400, CV_8UC3);
    createImage(image);
    showImage(image);


    getPixelValue(image,50,100);

 }

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::createImage(Mat &image){
    // Rechteck zeichnen
    Point linksOben(10, 20);
    Point rechtsUnten(231, 341);
    Scalar color(0, 0, 0);
    rectangle(image, linksOben, rechtsUnten, color, CV_FILLED);
}

void MainWindow::showImage(const Mat &image){
    // Mat Objekt darstellen
    QPixmap pixmap = cvMatToQPixmap(image);
    ui->imageLabel->setPixmap(pixmap);

}

void MainWindow::getPixelValue(Mat &image, int y, int x){

    //get rgb values of a pixel in a given image
    int red = image.at<Vec3b>(y,x)[2];
    int green = image.at<Vec3b>(y,x)[1];
    int blue = image.at<Vec3b>(y,x)[0];
    //display values in a Qlabel
    ui->Label->setText("red: "+QString::number(red)+" green: "+ QString::number(green)+" blue: "+QString::number(blue));
}


