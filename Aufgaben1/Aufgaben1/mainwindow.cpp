#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <iostream>
#include <opencv2/opencv.hpp>
using namespace cv;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::showMat(const Mat &mat){
    // Mat Objekt darstellen
    QImage image( mat.data, mat.cols, mat.rows, mat.step, QImage::Format_RGB888 );
    image = image.rgbSwapped();

    QPixmap pixmap = QPixmap::fromImage(image);
    ui->imageLabel->setPixmap(pixmap);

}



void MainWindow::on_actiondots_triggered(){
    Mat image(400,400,CV_8UC3);
    int radius = 12;
    const int countDots = 3000;
    for(int x = 0; x<countDots; x++){
        //random colour
        int red = rand() % 255;
        int green = rand() % 255;
        int blue = rand() % 255;
        Scalar randomColor(blue,green,red);
        //random position
        int xPos = rand() % 410 ;
        int yPos = rand() % 410 ;
        Point center(xPos,yPos);
        //draw cirlce
        circle(image, center, radius, randomColor, CV_FILLED);

    }
    showMat(image);
}

void MainWindow::on_actioncheckers_triggered(){
    Mat image(400,400,CV_8UC3);

    int checkerSize = 20;
    const Scalar colorBlack(0,0,0);
    const Scalar colorWhite(255,255,255);

    image = colorWhite;

    for(int x = 0; x < image.cols; x+=checkerSize){
        for(int y = 0; y < image.rows; y+=checkerSize){
            if((x+y) % (2*checkerSize)== 0){
            Point p1(x,y);
            Point p2(x+checkerSize,y+checkerSize);

            rectangle(image,p1,p2,colorBlack, CV_FILLED);
            }
        }
    }
    showMat(image);
}

void MainWindow::on_actioncircles_triggered(){
    Mat image(400,400,CV_8UC3);
    image = Scalar(255,255,255);
    Point center(image.cols/2,image.rows/2);
    const Scalar colorBlack(0,0,0);
    for(int radius = 20; radius <200; radius += 20){
        circle(image, center, radius, colorBlack);
    }
    showMat(image);
}
